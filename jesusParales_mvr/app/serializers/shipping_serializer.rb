class ShippingSerializer < ActiveModel::Serializer
  attributes :sender, :receiver, :paths, :office, :freight, :total_additional_charges

end
