class ClientSerializer < ActiveModel::Serializer
  attributes :ci, :full_mame
  has_many :sent_shippings
  
  def full_mame
    "#{object.name} #{object.lastname}"
  end 

end
