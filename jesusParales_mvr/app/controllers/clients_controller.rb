class ClientsController < ApplicationController
    protect_from_forgery with: :null_session
    
    def index
        @clients = Client.all 
        render json: !@clients.empty? ? @clients: {error: "no hay usuarios cargados"}
    end 

  
    def show 
    
    end 
    
    def create 
        begin 
            @client = Client.create(client_params) 
            render json: @client
        rescue ActiveRecord::RecordNotCreated => e
            render json: {error: e.message}
        end 
    end 

    def new
    end

    def  edit
    end 
    
    def destroy
    end 

    private 

    def client_params 
        params.require(:client).permit(:name, :lastname, :ci, :phone, :mobile)
    end 

end
