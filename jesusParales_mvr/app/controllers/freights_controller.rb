class FreightsController < ApplicationController
    protect_from_forgery with: :null_session

    def index
        @freights = Freight.all
        render json: @freights ? @freights : {error: "you have no freights"}
    end    

    def create 
        @freight = Freight.create (freight_params)
        render json: @freight.persisted? ?  @freight : {error: "I couldn't save this freight"}
    end 
    
    private 

    def freight_params
       params.require(:freight).permit(:kind, :weight, :description, :shipping_id)
    end  

end
