class OfficesController < ApplicationController
    protect_from_forgery with: :null_session
    
    def index 
        
    end 

    def create 
        @office = Office.create(office_params)
        render json: @office.persisted? ? @office : {error: "this office couldn't be saved"}
    end 


    private 
    
    def office_params 
        params.require(:office).permit(:name, :phone, :address, :location_id)
    end 

end
