class PathsController < ApplicationController
    protect_from_forgery with: :null_session 
    before_action :shipping_around

    def index
        @paths =  @shipping.paths
        render json: @paths
    end 

    def create
        @path = Path.new path_params 
        @path.save
        unless !@path.persisted? 
            if @shipping.paths.size > 1 
                paths = @shipping.paths
                last_path = paths[paths.size-2]
                last_path.estatus = 1
                last_path.save
                render json: @path
            end
        else
            render json: { error: "couldn't save this path"}
        end
    end 

    
    private 

    def path_params
        params.require(:path).permit(:date, :arrival_date, :price, :estatus, :shipping_id)
    end 

    def shipping_around
        @shipping = Shipping.find params[:shipping_id]
    end 

end
