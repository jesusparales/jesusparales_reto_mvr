class ShippingsController < ApplicationController
    protect_from_forgery with: :null_session
    before_action :shipping_around,  except: [:index, :create]
    
    def index
        @all = Shipping.all 
        render json: @all ?  @all  : { error: "you have no shippings now"}
    end

    def show
        additional = @shipping.paths.last if @shipping.paths.size > 1 && @shipping.paths.last.estatus == 0
        render json: @shipping ? @shipping : {message: "this isn't a valid shipping"}    
    end
    
    def create
       @shipping = Shipping.new
       @shipping.status = "sent"
       @shipping.total_price = shipping_params[:total_price]
       @shipping.pickup_date = DateTime.now
       @shipping.order_date  = DateTime.now
       @sender = Client.find shipping_params[:sender_id]
       @shipping.sender = @sender 
       @receiver = Client.find shipping_params[:receiver_id]
       @shipping.receiver = @receiver
       @office = Office.find shipping_params[:office_id]
       @shipping.office = @office
       @shipping.save
       render json: @shipping
    end 

    def new

    end
 
    def update 
        @shipping.pickup_date = shipping_params[:pickup_date] 
        @shipping.status = shipping_params[:status]
        @shipping.save
        render json: @shipping.persisted? ? { message:"saved!", user: @shipping}:{ message: "couldn't be save"}
    end 
        
    def  edit
    end 
    
    def destroy
    end 

    private 

    def shipping_params
        params.require(:shipping).permit(:total_price, :receiver_id, :sender_id, :office_id, :pickup_date, :status) 
    end 
    def shipping_around
        @shipping = Shipping.find params[:id]
    end 


end
