class Shipping < ApplicationRecord
  belongs_to :receiver, foreign_key: :receiver_id, class_name: "Client"
  belongs_to :sender, foreign_key: :sender_id, class_name: "Client"
  belongs_to :office
  has_many :paths
  belongs_to :freight

  enum status: [:pending, :sent, :received, :delivered]



   def total_additional_charges 
    if paths.size > 1
      total = 0 
      paths.each do |path|
        total += path.price 
      end 
      total -= paths.first.price 
    else 
      total=0 
    end
    total
  end
  
end
