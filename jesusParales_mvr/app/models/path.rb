class Path < ApplicationRecord
  belongs_to :shipping


  enum status: [:current, :delivered]
end
