# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


location_list = [ ["Amazonas", 0], ["Anzoátegui", 0], ["Apure", 0], 
                  ["Aragua", 0], ["Barinas", 0], ["Bolívar", 0], 
                  ["Carabobo", 0], ["Cojedes", 0], ["Delta Amacuro", 0], 
                  ["Distrito Capital", 0], ["Falcón", 0], ["Guárico", 0], 
                  ["Lara", 0], ["Mérida", 0], ["Miranda", 0], ["Monagas", 0], 
                  ["Nueva Esparta", 0], ["Portuguesa", 0], ["Sucre", 0], 
                  ["Táchira", 0], ["Trujillo", 0], ["Vargas", 0], 
                  ["Yaracuy", 0], ["Zulia", 0], ["Puerto Ayacucho", 1], 
                  ["Barcelona", 1], ["San Fernando de Apure", 1], 
                  ["Maracay", 1], ["Barinas", 1], ["Ciudad Bolívar", 1], 
                  ["Valencia", 1], ["San Carlos", 1], ["Tucupita", 1], 
                  ["Caracas", 1], ["Coro", 1], ["San Juan de Los Morros", 1], 
                  ["Barquisimeto", 1], ["Mérida", 1], ["Los Teques", 1], 
                  ["Maturín", 1], ["La Asunción", 1], ["Guanare", 1], 
                  ["Cumaná", 1], ["San Cristóbal", 1], ["Trujillo", 1], 
                  ["La Guaira", 1], ["San Felipe", 1], ["Maracaibo", 1] ]


                  location_list.each do |location,kind|
                    Location.create name: location, kind: kind
                  end