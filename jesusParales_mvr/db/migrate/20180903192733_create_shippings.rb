class CreateShippings < ActiveRecord::Migration[5.1]
  def change
    create_table :shippings do |t|
      t.datetime :order_date
      t.decimal :total_price
      t.datetime :pickup_date
      t.integer :status
      t.references :receiver, foreign_key: {to_table: :clients}, null: false
      t.references :sender, foreign_key: {to_table: :clients}, null: false
      t.references :office, foreign_key: true

      t.timestamps
    end
  end
end
