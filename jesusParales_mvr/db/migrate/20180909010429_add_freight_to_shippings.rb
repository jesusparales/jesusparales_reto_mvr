class AddFreightToShippings < ActiveRecord::Migration[5.1]
  def change
    add_reference :shippings, :freight, foreign_key: true
  end
end
