class Freights < ActiveRecord::Migration[5.1]
  def change
    def change
      create_table :paths do |t|
        t.integer :kind 
        t.decimal :arrival_date
        t.string  :price
        
        t.timestamps
      end
    end
  end
end
