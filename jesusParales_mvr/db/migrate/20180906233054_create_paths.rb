class CreatePaths < ActiveRecord::Migration[5.1]
  def change
    create_table :paths do |t|
      t.date :date
      t.date :arrival_date
      t.decimal :price
      t.integer :estatus
      t.references :shipping, foreign_key: true

      t.timestamps
    end
  end
end
