class CreateOffices < ActiveRecord::Migration[5.1]
  def change
    create_table :offices do |t|
      t.string :name
      t.string :phone
      t.text :address
      t.references :location, foreign_key: true

      t.timestamps
    end
  end
end
