class AddDetailsToFreights < ActiveRecord::Migration[5.1]
  def change
    add_column :freights, :kind, :string
    add_column :freights, :wight, :decimal
    add_column :freights, :description, :string
  end
end
