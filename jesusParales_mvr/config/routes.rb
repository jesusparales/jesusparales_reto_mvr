Rails.application.routes.draw do
  get 'test/resource'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
resources :clients
resource :offices
resources :freights

resources :shippings, shallow: true do 
  resources :paths
  
 collection do 
  get :search, to: 'shippingsearch#index'
 end 
       
end 

end
